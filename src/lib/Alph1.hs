{-# LANGUAGE OverloadedStrings, BangPatterns, ScopedTypeVariables #-}
module Alph1 (byFirst, bySecond, buildTable, reportTable, alph1) where
import Data.List
import Data.Conduit as C
import Data.Char
import Text.Printf
import Text.Regex.PCRE
import Data.Array ((!))
import Control.Monad
import Control.Monad.Trans.Resource
import System.Environment (getArgs)
import qualified Data.Text as T
import qualified Data.Text.Encoding as T
import qualified Data.HashMap.Strict as HM
import qualified Data.Conduit.List as CL
import qualified Data.ByteString.Char8 as BS
import qualified Data.Conduit.Binary as CB

byFirst :: (Ord a) => (a, b) -> (a, b) -> Ordering
byFirst (!a, !_) (!b, !_) = a `compare` b

bySecond :: (Ord b) => (a, b) -> (a, b) -> Ordering
bySecond (!_, !a) (!_, !b) = a `compare` b


wordsel :: Regex
wordsel = makeRegex ("\\w+" :: BS.ByteString)

capitalize :: T.Text -> T.Text
capitalize word 
  | Just (c, r) <- T.uncons word =
        T.cons (toUpper c) r
  | otherwise =
        word

buildTable :: (Num b, Monad m) => HM.HashMap T.Text b  -> ConduitT BS.ByteString Void m (HM.HashMap T.Text b)
buildTable table =
    CL.concatMap (map (capitalize . T.decodeUtf8 . fst . (!0)) . matchAllText wordsel) .|
    CL.fold (\m w -> HM.insertWith (+) w 1 m) table

reportTable :: (PrintfArg a, PrintfArg b) => ((a, b) -> (a, b) -> Ordering) -> HM.HashMap a b -> IO ()
reportTable f =
    mapM_ (\(k, v) -> printf "%24s: %10d\n" k v) . sortBy f . HM.toList

alph1 :: (Real a, PrintfArg a) => a -> IO ()
alph1 (_ :: a) = do
    args <- getArgs
    let cmp =
          case args of
              ("-a":_) -> byFirst
              _        -> bySecond
    forM_ [1..100] $ \i -> do
        printf "# Begin Iteration %d\n" (i :: Int)
        table <- runResourceT $ runConduit $ 
          CB.sourceFile "TEXT-PCE-127.txt" .|
          CB.lines .|
          buildTable HM.empty
        reportTable cmp (table :: HM.HashMap T.Text a)
        printf "# End Iteration %d\n" i
