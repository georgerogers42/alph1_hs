{-# LANGUAGE OverloadedStrings, BangPatterns #-}
module Main where
import Alph1

main :: IO ()
main = alph1 (0 :: Int)
